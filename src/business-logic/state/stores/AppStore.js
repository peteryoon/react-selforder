import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise-middleware';
import { composeWithDevTools } from 'redux-devtools-extension';

import { AppReducer } from '../reducers/AppReducer';

export const createAppStore = () => {
  return createStore(
    AppReducer,
    /* preloadedState, */ composeWithDevTools(
      applyMiddleware(thunk, promiseMiddleware())
    )
  );
};
