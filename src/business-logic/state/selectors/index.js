function getCategories(state) {
  return state.main.categories;
}

function getProducts(state) {
  return state.main.products;
}

function getCurrentCategory(state) {
  return state.main.current.category;
}

function getCurrentProduct(state) {
  return state.main.current.product;
}

function getCurrentModifier(state) {
  return state.main.optionset;
}

function getOrder(state) {
  return state.main.order;
}

function getOrderItem(state) {
  const numItems = state.main.order.items.length;

  return state.main.order.items[numItems - 1];
}

function getUpsell(state) {
  return state.main.upsell;
}

export {
  getCategories,
  getCurrentCategory,
  getCurrentProduct,
  getCurrentModifier,
  getOrder,
  getOrderItem,
  getProducts,
  getUpsell
};
