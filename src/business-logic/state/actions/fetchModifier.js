import { FETCH_MODIFIER } from './actionTypes';
import { apiBaseUrl } from '../../config';

function fetchModifier(productId, modifierIndex) {
  return dispatch => {
    fetch(`${apiBaseUrl}order/getProduct.php`, {
      method: 'post',
      body: JSON.stringify({
        id: productId,
        id2: modifierIndex
      })
    })
      .then(res => res.json())
      .then(res => {
        const optionset = res.options[+modifierIndex] || {};
        dispatch({
          type: FETCH_MODIFIER,
          payload: {
            product: res.product,
            optionset,
            modifierIndex
          }
        });
      });
  };
}

export default fetchModifier;
