import { CHANGE_QUANTITY } from './actionTypes';

function changeQuantity(quantity) {
  return dispatch => {
    return dispatch({
      type: CHANGE_QUANTITY,
      payload: quantity
    });
  };
}

export default changeQuantity;
