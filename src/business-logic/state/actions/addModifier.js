import { ADD_MODIFIER } from './actionTypes';

function addModifier(modifierIndex, optionset) {
  return dispatch => {
    return dispatch({
      type: ADD_MODIFIER,
      payload: {
        modifierIndex: +modifierIndex,
        optionset
      }
    });
  };
}

export default addModifier;
