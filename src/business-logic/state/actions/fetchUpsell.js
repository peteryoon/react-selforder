import { FETCH_UPSELL } from './actionTypes';
import { apiBaseUrl } from '../../config';

function fetchUpsell() {
  return dispatch => {
    fetch(`${apiBaseUrl}order/getUpsell.php`, {
      method: 'post'
    })
      .then(res => res.json())
      .then(res => {
        console.log('RESPONSE', res);
        dispatch({
          type: FETCH_UPSELL,
          payload: res.upsells
        });
      });
  };
}

export default fetchUpsell;
