import { RESET_ALL } from './actionTypes';

function resetAll() {
  return dispatch => {
    return dispatch({
      type: RESET_ALL
    });
  };
}

export default resetAll;
