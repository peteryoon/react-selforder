import { FETCH_CATEGORY } from './actionTypes';
import { apiBaseUrl } from '../../config';

function fetchCategory(categoryId) {
  return dispatch => {
    fetch(`${apiBaseUrl}order/getCategory.php`, {
      method: 'post',
      body: JSON.stringify({
        id: categoryId
      })
    })
      .then(res => res.json())
      .then(res => {
        console.log('RESPONSE', res);
        dispatch({
          type: FETCH_CATEGORY,
          payload: {
            products: res.products,
            categories: res.tree,
            root: res.root
          }
        });
      });
  };
}

export default fetchCategory;
