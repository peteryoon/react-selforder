import { SELECT_PRODUCT } from './actionTypes';

function selectProduct(product) {
  return dispatch => {
    return dispatch({
      type: SELECT_PRODUCT,
      payload: product
    });
  };
}

export default selectProduct;
