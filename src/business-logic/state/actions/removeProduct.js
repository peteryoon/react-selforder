import { REMOVE_PRODUCT } from './actionTypes';

function removeProduct() {
  return dispatch => {
    return dispatch({
      type: REMOVE_PRODUCT
    });
  };
}

export default removeProduct;
