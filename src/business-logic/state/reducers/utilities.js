// Get total price from order
export function deriveOrderPrice(order) {
  if (!order.items) return { ...order, subTotal: 0, tax: 0, grandTotal: 0 };

  const subTotal = order.items.reduce(
    (total, item) => total + item.subTotal * item.quantity,
    0
  );
  const tax = order.items.reduce(
    (total, item) => total + item.taxTotal * item.quantity,
    0
  );
  const grandTotal = subTotal + tax;

  return {
    ...order,
    subTotal,
    tax,
    grandTotal
  };
}

export function deriveItemPrice(item) {
  const modifierPrice = item.optionsets.reduce(
    (total, modifier) => total + (modifier.subTotal || 0),
    0
  );
  const subTotal = item.oprice + modifierPrice;
  const taxTotal = (subTotal * item.tax) / 100;

  return {
    ...item,
    subTotal,
    taxTotal
  };
}

export function deriveModifierPrice(modifier) {
  const subTotal = modifier.options.reduce(
    (total, option) => total + option.p,
    0
  );

  return {
    ...modifier,
    subTotal
  };
}
