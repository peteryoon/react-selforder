import {
  FETCH_CATEGORY,
  SELECT_PRODUCT,
  FETCH_MODIFIER,
  CHANGE_QUANTITY,
  ADD_MODIFIER,
  REMOVE_PRODUCT,
  FETCH_UPSELL,
  RESET_ALL
} from '../actions/actionTypes';
import {
  deriveOrderPrice,
  deriveItemPrice,
  deriveModifierPrice
} from './utilities';

const initialState = {
  current: {},
  order: {
    items: []
  },
  upsell: null,
  categories: [],
  products: [],
  optionset: null
};

function addNewOrder(order, product) {
  const newOrder = {
    ...order,
    items: [...order.items, product]
  };

  return deriveOrderPrice(newOrder);
}

function changeQuantity(order, quantity) {
  const newOrder = { ...order };
  const item = newOrder.items[newOrder.items.length - 1];
  item.quantity = quantity;

  return deriveOrderPrice(newOrder);
}

function addModifier(order, { modifierIndex, optionset }) {
  const newOrder = { ...order };
  let item = newOrder.items[newOrder.items.length - 1];
  item.optionsets[modifierIndex] = deriveModifierPrice(optionset);

  newOrder.items = newOrder.items.map(item => {
    return deriveItemPrice(item);
  });

  return deriveOrderPrice(newOrder);
}

function adjustOrderModifier(order, modifierIndex) {
  const newOrder = { ...order };
  const item = newOrder.items[newOrder.items.length - 1];
  const numIndex = +modifierIndex + 1;

  if (!item) return order;

  if (item.optionsets && item.optionsets.length) {
    item.optionsets = item.optionsets.slice(0, numIndex);
  }

  return deriveOrderPrice(newOrder);
}

function removeProduct(order) {
  const newOrder = { ...order };

  newOrder.items.length = newOrder.items.length - 1;

  return deriveOrderPrice(newOrder);
}

export const MainReducer = (state = initialState, action) => {
  switch (action.type) {
    case RESET_ALL:
      return { ...initialState };
    case FETCH_CATEGORY:
      return {
        ...state,
        categories: action.payload.categories,
        products: action.payload.products,
        optionset: null,
        current: {
          ...state.current,
          category: action.payload.root
        }
      };
    case FETCH_MODIFIER:
      return {
        ...state,
        optionset: action.payload.optionset,
        current: {
          ...state.current,
          product: action.payload.product
        },
        order: adjustOrderModifier(state.order, action.payload.modifierIndex)
      };
    case SELECT_PRODUCT:
      return {
        ...state,
        current: {
          ...state.current,
          product: action.payload
        },
        order: addNewOrder(state.order, action.payload)
      };
    case CHANGE_QUANTITY:
      return {
        ...state,
        order: changeQuantity(state.order, action.payload)
      };
    case ADD_MODIFIER:
      return {
        ...state,
        order: addModifier(state.order, action.payload)
      };
    case REMOVE_PRODUCT:
      return {
        ...state,
        order: removeProduct(state.order)
      };
    case FETCH_UPSELL:
      return {
        ...state,
        upsell: action.payload
      };
    default:
      return state;
  }
};
