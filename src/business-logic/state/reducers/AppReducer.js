import { combineReducers } from 'redux';

import { MainReducer } from './mainReducer';

export const AppReducer = combineReducers({
  main: MainReducer
});
