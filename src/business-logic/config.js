export const apiBaseUrl =
  process.env.NODE_ENV === 'development'
    ? 'http://localhost/selforder/api/'
    : 'http://peteryoon.com/selforder/api/';
