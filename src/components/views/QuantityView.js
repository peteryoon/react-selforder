import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

class QuantityView extends React.Component {
  constructor(props) {
    super(props);

    this.state = { quantity: 1 };

    this.changeQuantity = this.changeQuantity.bind(this);
  }

  changeQuantity(number) {
    const temp = this.state.quantity + number;
    const quantity = temp < 1 ? 1 : temp;

    this.setState({
      quantity
    });

    this.props.onQuantityChange(quantity);
  }

  render() {
    const { quantity } = this.state;

    return (
      <div>
        <div id="header">
          <h2>Select Quantity</h2>
        </div>
        <div className="prod-container">
          <div className="quantity vcenter center">
            <i
              className="fa fa-arrow-down"
              onClick={() => this.changeQuantity(-1)}
            ></i>
            <input
              type="tel"
              className="quantity"
              readOnly={true}
              value={quantity}
            />
            <i
              className="fa fa-arrow-up"
              onClick={() => this.changeQuantity(1)}
            ></i>
          </div>
        </div>
        <div className="category option next">
          <Link to={'/home/review'}>
            <div className="inner-box">
              <i className="far fa-arrow-alt-circle-right"></i>
              <div>Continue</div>
            </div>
          </Link>
        </div>
      </div>
    );
  }
}

QuantityView.propTypes = {
  onQuantityChange: PropTypes.func
};

export default QuantityView;
