import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import { fetchUpsell } from '../../business-logic/state/actions';
import { getUpsell } from '../../business-logic/state/selectors';

import { CategoryBox } from '../shared';

class ReviewView extends React.Component {
  componentDidMount() {
    const { fetchUpsellAction } = this.props;

    fetchUpsellAction();
  }

  render() {
    const { upsell } = this.props;

    if (!upsell) return null;

    return (
      <div>
        <div id="header">
          <h2>Add a Side</h2>
        </div>
        <div className="prod-container upsell center">
          {upsell.map((category, i) => (
            <CategoryBox key={i} category={category} />
          ))}
        </div>
        <div className="center">
          <div className="clear center">DID YOU FORGET SOMETHING?</div>
          <Link to="/home/category/1">
            <button className="another btn btn-lg btn-warning btn-block">
              Select Another Item
            </button>
          </Link>
        </div>

        <div id="footer" className="review">
          <div className="heretogo center">
            <div>READY TO COMPLETE YOUR ORDER?</div>
          </div>
          <Link to="/starter">
            <button className="shadow noborder complete btn btn-lg btn-warning btn-block">
              Complete and Print My Order
            </button>
          </Link>
        </div>
      </div>
    );
  }
}

ReviewView.propTypes = {
  upsell: PropTypes.arrayOf(PropTypes.object),
  fetchUpsellAction: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  upsell: getUpsell(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({ fetchUpsellAction: fetchUpsell }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ReviewView);
