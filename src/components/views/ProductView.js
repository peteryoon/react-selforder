import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import {
  fetchModifier,
  addModifier,
  removeModifier,
  changeQuantity
} from '../../business-logic/state/actions';
import {
  getCurrentProduct,
  getCurrentModifier,
  getOrder,
  getOrderItem
} from '../../business-logic/state/selectors';
import { ModifierBox } from '../shared';
import QuantityView from './QuantityView';

class ProductView extends React.Component {
  constructor(props) {
    super(props);

    this.onClickModifier = this.onClickModifier.bind(this);
    this.onQuantityChange = this.onQuantityChange.bind(this);
  }

  componentDidMount() {
    console.log('PRODUCT VIEW MOUNT');
    const { match, fetchModifierAction } = this.props;
    const { productId, modifierIndex } = match.params;

    if (productId) {
      fetchModifierAction(productId, modifierIndex);
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { match, fetchModifierAction } = this.props;
    const { productId: prevId, modifierIndex: prevIndex } = match.params;
    const {
      productId: newId,
      modifierIndex: newIndex
    } = nextProps.match.params;

    if (newId !== prevId || prevIndex !== newIndex) {
      if (+newIndex < prevIndex) {
        // TODO: Remove Item
      }
      fetchModifierAction(newId, newIndex);
    }
  }

  onClickModifier(index) {
    const {
      optionset,
      addModifierAction,
      orderItem,
      match,
      history
    } = this.props;
    const { productId, modifierIndex } = match.params;
    const modifier = optionset.options[index];
    const nextModifierIndex = +modifierIndex + 1;

    if (modifier) {
      // Multi. Toggle selection
      if (optionset.multi) {
        const orderOptionset = orderItem.optionsets[modifierIndex]
          ? { ...orderItem.optionsets[modifierIndex] }
          : { ...optionset, selections: [], options: [] };
        const selections = orderOptionset.selections
          ? [...orderOptionset.selections]
          : [];
        let newOptionset;

        // Remove/Unselect
        if (selections.indexOf(index) >= 0) {
          selections.splice(index, 1);
          newOptionset = {
            ...orderOptionset,
            selections,
            options: orderOptionset.options.filter(
              option => option.id !== modifier.id
            )
          };
        }
        // Add/Select
        else {
          selections.push(index);
          newOptionset = {
            ...orderOptionset,
            selections,
            options: [...orderOptionset.options, modifier]
          };
        }

        addModifierAction(modifierIndex, newOptionset);
      } else {
        // Single Regular Selection
        const newOptionset = {
          ...optionset,
          options: [modifier],
          selections: [index]
        };
        addModifierAction(modifierIndex, newOptionset);

        history.push(`/home/product/${productId}/${nextModifierIndex}`);
      }
    }
  }

  onQuantityChange(quantity) {
    this.props.changeQuantityAction(quantity);
  }

  render() {
    const { product, optionset, orderItem, match } = this.props;
    const { modifierIndex } = match.params;
    const nextModifierIndex = +modifierIndex + 1;

    // If there is no more modifier then return quantity view
    if (optionset === null) return null;

    if (!optionset.id) {
      return <QuantityView onQuantityChange={this.onQuantityChange} />;
    }

    const selections =
      orderItem.optionsets[modifierIndex] &&
      orderItem.optionsets[modifierIndex].selections
        ? orderItem.optionsets[modifierIndex].selections
        : [];
    const title = optionset.desc || optionset.name;

    return (
      <div className="fullbox">
        <div id="header">
          <h2>{title}</h2>
        </div>
        <div className="prod-container modifier center">
          {optionset.options.map((modifier, i) => (
            <ModifierBox
              key={i}
              modifier={modifier}
              isSelected={selections && selections.indexOf(i) >= 0}
              onClickModifier={() => this.onClickModifier(i)}
            />
          ))}
        </div>
        {optionset.multi && (
          <div className="category option next">
            <Link to={`/home/product/${product.id}/${nextModifierIndex}`}>
              <div className="inner-box">
                <i className="far fa-arrow-alt-circle-right"></i>
                <div>Continue</div>
              </div>
            </Link>
          </div>
        )}
      </div>
    );
  }
}

ProductView.propTypes = {
  match: PropTypes.object,
  product: PropTypes.object,
  optionset: PropTypes.object,
  order: PropTypes.object,
  orderItem: PropTypes.object,
  history: PropTypes.object,
  fetchModifierAction: PropTypes.func,
  addModifierAction: PropTypes.func,
  removeModifierAction: PropTypes.func,
  changeQuantityAction: PropTypes.func
};

const mapStateToProps = state => ({
  product: getCurrentProduct(state),
  optionset: getCurrentModifier(state),
  order: getOrder(state),
  orderItem: getOrderItem(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchModifierAction: fetchModifier,
      addModifierAction: addModifier,
      removeModifierAction: removeModifier,
      changeQuantityAction: changeQuantity
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ProductView);
