import CategoryView from './CategoryView';
import ProductView from './ProductView';
import QuantityView from './QuantityView';
import ReviewView from './ReviewView';

export { CategoryView, ProductView, QuantityView, ReviewView };
