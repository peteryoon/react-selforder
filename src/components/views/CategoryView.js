import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { withLastLocation } from 'react-router-last-location';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import {
  fetchCategory,
  selectProduct,
  removeProduct
} from '../../business-logic/state/actions';
import {
  getCategories,
  getProducts,
  getCurrentCategory
} from '../../business-logic/state/selectors';
import { CategoryBox, ProductBox } from '../shared';

class CategoryView extends React.Component {
  constructor(props) {
    super(props);

    this.onSelectProduct = this.onSelectProduct.bind(this);
  }

  componentDidMount() {
    const {
      match,
      fetchCategoryAction,
      lastLocation,
      removeProductAction
    } = this.props;
    const { categoryId } = match.params;

    if (categoryId) {
      fetchCategoryAction(categoryId);
    }

    // TODO: If came from back (product screen) then pop the last item
    console.log('LAST LOCATION', lastLocation);
    if (lastLocation && lastLocation.pathname.includes('product')) {
      removeProductAction();
    }
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    const { match, fetchCategoryAction } = this.props;
    const { categoryId: prevId } = match.params;
    const newId = nextProps.match.params.categoryId;

    if (newId !== prevId) {
      fetchCategoryAction(newId);
    }
  }

  onSelectProduct(product) {
    const { selectProductAction } = this.props;
    console.log('PRODUCT SELECTED', product);
    if (product) {
      selectProductAction({
        id: product.id,
        groupId: product.groupId,
        name: product.name,
        oprice: product.price,
        quantity: 1,
        subTotal: product.price,
        tax: product.tax,
        taxTotal: (product.price * product.tax) / 100,
        optionsets: []
      });

      this.props.history.push(`/home/product/${product.id}/0`);
    }
  }

  render() {
    const { categories, category, products } = this.props;

    if (!category) return null;

    return (
      <div>
        <div id="header">
          <h2>{category.name}</h2>
        </div>
        <div className="prod-container center">
          {categories.map((category, i) => (
            <CategoryBox key={i} category={category} />
          ))}
          {products.map((product, i) => (
            <ProductBox
              key={i}
              product={product}
              onSelectProduct={() => this.onSelectProduct(product)}
            />
          ))}
        </div>
      </div>
    );
  }
}

CategoryView.propTypes = {
  category: PropTypes.object,
  categories: PropTypes.arrayOf(PropTypes.object),
  fetchCategoryAction: PropTypes.func.isRequired,
  match: PropTypes.object,
  history: PropTypes.object,
  lastLocation: PropTypes.object,
  products: PropTypes.arrayOf(PropTypes.object),
  selectProductAction: PropTypes.func.isRequired,
  removeProductAction: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  categories: getCategories(state),
  category: getCurrentCategory(state),
  products: getProducts(state)
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchCategoryAction: fetchCategory,
      selectProductAction: selectProduct,
      removeProductAction: removeProduct
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(withLastLocation(CategoryView)));
