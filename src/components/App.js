import React from 'react';
import { Provider } from 'react-redux';

import { createAppStore } from '../business-logic/state/stores/AppStore';
import { AppRouter } from './routers/AppRouter';

export const App = () => (
  <Provider store={createAppStore()}>
    <div id="main-container">
      <AppRouter />
    </div>
  </Provider>
);
