import React from 'react';
import { PropTypes } from 'prop-types';

class ProductBox extends React.Component {
  render() {
    const { product, onSelectProduct } = this.props;
    const price = '$' + product.price.toFixed(2);

    return (
      <div className="shadow product box" onClick={onSelectProduct}>
        <div className="text-container">
          <div className="name">
            <div className="vcenter">{product.name}</div>
          </div>
        </div>
        <div className="price-container">
          <div className="vcenter">{price}</div>
        </div>
      </div>
    );
  }
}

ProductBox.propTypes = {
  product: PropTypes.object.isRequired,
  onSelectProduct: PropTypes.func.isRequired
};

export default ProductBox;
