import React from 'react';
import { PropTypes } from 'prop-types';

class ModifierBox extends React.Component {
  render() {
    const { modifier, isSelected, onClickModifier } = this.props;
    const price = modifier.p > 0 ? '$' + modifier.p.toFixed(2) : '-------';

    return (
      <div className="shadow product box" onClick={onClickModifier}>
        {isSelected && (
          <div className="checks">
            <i className="fa fa-check"></i>
          </div>
        )}
        <div className="text-container">
          <div className="name">
            <div className="vcenter">{modifier.n}</div>
          </div>
        </div>
        <div className="price-container">
          <div className="vcenter">{price}</div>
        </div>
      </div>
    );
  }
}

ModifierBox.propTypes = {
  isSelected: PropTypes.bool.isRequired,
  modifier: PropTypes.object.isRequired,
  onClickModifier: PropTypes.func.isRequired
};

export default ModifierBox;
