import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { getOrder } from '../../business-logic/state/selectors';

class ReviewView extends React.Component {
  formatMoney(price) {
    return '$' + price.toFixed(2);
  }

  render() {
    const { order } = this.props;

    console.log('RECEIPT', order);

    return (
      <div>
        <h3>Your Order</h3>
        <div id="receipt" className="paper">
          <div className="inner">
            {order.items.map((item, i) => (
              <div key={i} className="item">
                <span className="pull-right price">
                  {this.formatMoney(item.oprice * item.quantity)}
                </span>
                <div className="item-name">
                  {item.name}
                  {item.quantity > 1 && <span> x{item.quantity}</span>}
                </div>
                {item.optionsets.map((optionset, j) => (
                  <div key={j}>
                    {optionset.options.map((option, k) => (
                      <div key={k} className="optionset">
                        <span className="pull-right price">
                          {this.formatMoney(option.p)}
                        </span>
                        <div className="option-name">{option.n}</div>
                      </div>
                    ))}
                  </div>
                ))}
              </div>
            ))}
            {order.grandTotal > 0 && (
              <div className="totals">
                <div>
                  <span className="pull-right price">
                    {this.formatMoney(order.subTotal)}
                  </span>
                  <div className="subtotal">Subtotal:</div>
                </div>
                <div>
                  <span className="pull-right price">
                    {this.formatMoney(order.tax)}
                  </span>
                  <div className="tax">Tax:</div>
                </div>
                <div className="grandtotal">
                  <span className="pull-right price">
                    {this.formatMoney(order.grandTotal)}
                  </span>
                  <div className="subtotal">Grand Total:</div>
                  <br />
                  <br />
                  <br />
                </div>
              </div>
            )}
          </div>
          <div className="side-buttons scroll-control">
            <div>
              <i className="far fa-arrow-alt-circle-up"></i>
            </div>
            <div>
              <i className="far fa-arrow-alt-circle-down"></i>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

ReviewView.propTypes = {
  order: PropTypes.object
};

const mapStateToProps = state => ({
  order: getOrder(state)
});

export default connect(mapStateToProps, null)(ReviewView);
