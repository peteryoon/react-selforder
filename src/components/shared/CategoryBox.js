import React from 'react';
import { PropTypes } from 'prop-types';

import { Link } from 'react-router-dom';

class CategoryBox extends React.Component {
  render() {
    const { category } = this.props;
    const { groupId } = category;
    let image;

    try {
      image = require(`../../assets/img/category/${groupId}.jpg`);
    } catch (e) {
      image = require('../../assets/img/placeholder.png');
    }

    return (
      <Link to={`/home/category/${groupId}`}>
        <div className="shadow category box">
          <div className="name">
            <div className="vcenter">{category.name}</div>
          </div>
          <div className="image">
            <img className="box-image" src={image} alt="" />
          </div>
        </div>
      </Link>
    );
  }
}

CategoryBox.propTypes = {
  category: PropTypes.object.isRequired
};

export default CategoryBox;
