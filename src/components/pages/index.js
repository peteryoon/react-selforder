import HomePage from './HomePage';
import StarterPage from './StarterPage';
import ThankYouPage from './ThankYouPage';

export { HomePage, StarterPage, ThankYouPage };
