import React from 'react';
import { Route, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import Receipt from '../shared/Receipt';
import { CategoryView, ProductView, QuantityView, ReviewView } from '../views';

class HomePage extends React.Component {
  constructor(props) {
    super(props);

    this.goBack = this.goBack.bind(this);
  }

  goBack() {
    this.props.history.goBack();
  }

  render() {
    return (
      <div id="home-container">
        <div id="view-container">
          <Route
            path={'/home/category/:categoryId'}
            component={CategoryView}
          ></Route>
          <Route
            path={'/home/product/:productId/:modifierIndex'}
            component={ProductView}
          ></Route>
          <Route path={'/home/quantity'} component={QuantityView}></Route>
          <Route path={'/home/review'} component={ReviewView}></Route>
        </div>
        <div id="receipt-container">
          <Receipt />
          <div className="side-buttons back-cancel" onClick={this.goBack}>
            <div className="noselect">
              <i className="far fa-arrow-alt-circle-left"></i>
              <div>Go Back</div>
            </div>

            <div className="noselect pull-right">
              <i className="far fa-times-circle"></i>
              <div>Cancel</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

HomePage.propTypes = {
  history: PropTypes.object
};

export default withRouter(HomePage);
