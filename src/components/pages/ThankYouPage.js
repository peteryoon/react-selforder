import React from 'react';
import { PropTypes } from 'prop-types';

import { withRouter } from 'react-router-dom';

class ThankYouPage extends React.Component {
  componentDidMount() {
    setTimeout(() => {
      this.props.history.push('/');
    }, 5000);
  }

  render() {
    return (
      <div id="thank-you">
        <div>Thank You!</div>
        <div className="details"></div>
      </div>
    );
  }
}

ThankYouPage.propTypes = {
  history: PropTypes.object
};

export default withRouter(ThankYouPage);
