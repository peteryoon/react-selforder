import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';

import { resetAll } from '../../business-logic/state/actions';

import welcomeImage from '../../assets/img/start.jpg';

class StarterPage extends React.Component {
  constructor(props) {
    super(props);

    console.log('Starter Page Contructor');
  }

  componentDidMount() {
    this.props.resetAction();
  }

  render() {
    return (
      <div>
        <Link to="/home/category/1">
          <div className="main-image">
            <img src={welcomeImage} />
          </div>
          <div id="footer">
            <button className="shadow noborder start btn btn-lg btn-block">
              Touch the Screen to Order
            </button>
          </div>
        </Link>
      </div>
    );
  }
}

StarterPage.propTypes = {
  resetAction: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      resetAction: resetAll
    },
    dispatch
  );

export default connect(null, mapDispatchToProps)(StarterPage);
