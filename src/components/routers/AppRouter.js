import React, { Fragment } from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { LastLocationProvider } from 'react-router-last-location';
import { HomePage, StarterPage, ThankYouPage } from '../pages';

export const AppRouter = () => (
  <BrowserRouter>
    <LastLocationProvider>
      <Fragment>
        <Switch>
          <Route path="/" component={StarterPage} exact={true} />
          <Route path="/home" component={HomePage} />
          <Route path="/thankyou" component={ThankYouPage} />
          <Redirect to="/" />
        </Switch>
      </Fragment>
    </LastLocationProvider>
  </BrowserRouter>
);
